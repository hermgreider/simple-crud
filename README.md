## Overview
This is a simple Create, Read, Update, Delete (CRUD) app using:
* Raspberry Pi as the hardware
* Docker to run Mongo
* MongoDB as the database
* NodeJS/ExpressJS with Typescript for REST API
* React for Front-end

## Setup Raspbian

### How to develop using a Pi
Using a Pi makes a clean environment to start from scratch. 

* Docker, Mongo, and app run on the Pi
* Files are on Pi
* Visual Studio Code running on Mac provides the IDE 
* Visual Studio Code Remote SSH tools connect to the Pi

### Raspbian Imager

Download and install Raspberry Pi Imager from: 
https://www.raspberrypi.com/software/

This creates the Linux image on the Pi's SD card.

Insert the SD card into your Mac or PC. Then, run the imager.

1. Select Pi hardware type, Pi OS Lite 64-bit, SD card
2. Edit Settings
    * Set hostname
    * Set user/pass
    * Set ssid/pass
    * Under services, enable SSH, either use password with user/pass or only use key
3. Save to Write 

### Start Pi
Eject the SD and insert into your Pi. Power up the Pi. 

A headless install of Raspbian is a little tricky. If it doesn't come up, you'll have to get a monitor on it. Otherwise, you should be able to ssh into the Pi from your computer at this point.

### Login to Pi
    ssh raspberrypi # on Mac may need raspberrypi.local

### Update Rasbian
    sudo apt update
    sudo apt full-upgrade

## Install Docker (Use apt install)
Reference:

https://docs.docker.com/engine/install/debian/
https://docs.docker.com/engine/install/debian#install-using-the-repository
    
### Add Docker's official GPG key:
    sudo apt-get update
    sudo apt-get install ca-certificates curl
    sudo install -m 0755 -d /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
    sudo chmod a+r /etc/apt/keyrings/docker.asc
    
### Add the repository to Apt sources:
    echo \
    "deb [arch=$(dpkg --print-architecture) \
    signed-by=/etc/apt/keyrings/docker.asc] \
    https://download.docker.com/linux/debian \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    sudo apt-get update
        
### Install Docker
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    
### Test Installation
    sudo docker run hello-world
    
### Create a non-root user group
Reference: https://docs.docker.com/engine/install/linux-postinstall/

    sudo usermod -aG docker $USER
    newgrp docker
    docker run hello-world

### Test docker-compose
    docker compose version

## Load Mongo Docker images

Default Mongo on pi 4 does not work according to:

https://forums.docker.com/t/mongo-db-raspberry-pi4/138908/2

Instead, we will use unofficial Pi releases

https://github.com/themattman/mongodb-raspberrypi-docker/tree/main

To load an unofficial release for Pi 4

    wget https://github.com/themattman/mongodb-raspberrypi-docker/releases/download/r7.0.6-mongodb-raspberrypi-docker-unofficial/mongodb.ce.pi4.r7.0.6-mongodb-raspberrypi-docker-unofficial.tar.gz
    
    docker load --input mongodb.ce.pi4.r7.0.6-mongodb-raspberrypi-docker-unofficial.tar.gz

## Setup git

    sudo apt install git
    git config --global user.name "FIRST_NAME LAST_NAME"
    git config --global user.email "MY_NAME@example.com"

## Install Node
Reference: https://blog.logrocket.com/how-to-set-up-node-typescript-express/

### Install Node Version Manager (nvm)
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh |bash

### source bashrc to get nvm on PATH
    . .bashrc

### Install node (long term support version)
    nvm install --lts
    nvm use --lts
    node --version

## Create Project

### Create Gitlab project
If using the existing repo, skip this step. Continue at "Setup Access Tokens."

On gitlab, create a new project simple-crud. Only readme.md in the project.

### Setup Access Tokens to Gitlab

On gitlab, Edit Profile, Access Tokens, Add New Token

    Token Name: greider.gitlab
    Scopes: api, write_repository, write_registry, ai_features 

### Clone the repo on Pi

    git clone https://greider.gitlab:<token>@gitlab.com/hermgreider/simple-crud.git

## Connect VSCode to Pi

VSCode with Remote SSH tools allows you to edit files on the 
Pi from your Mac or Windows PC. You can also open a terminal window to the Pi. When you start an application, it automatically
sets up a tunnel to the Pi to map localhost to raspberrypi.local. 

    1. Install VSCode on Mac or PC
    2. Install "Remote - SSH" extension 
    3. View, Command Palette, Connect to Host, raspberrypi.local
    4. Open a terminal 

## Setup Mongo in Docker

### Create a stack.yml 
Ref: https://hub.docker.com/_/mongo

If using existing repo, skip this and go to "Start the Stack."

Create a stack.yml to run in docker compose

    services:

    mongo:
        image: mongodb-raspberrypi4-unofficial-r7.0.6
        restart: always
        environment:
        MONGO_INITDB_ROOT_USERNAME: root
        MONGO_INITDB_ROOT_PASSWORD: example

    mongo-express:
        image: mongo-express
        restart: always
        ports:
        - 8081:8081
        environment:
        ME_CONFIG_MONGODB_ADMINUSERNAME: root
        ME_CONFIG_MONGODB_ADMINPASSWORD: example
        ME_CONFIG_BASICAUTH_USERNAME: admin
        ME_CONFIG_BASICAUTH_PASSWORD: admin123
        ME_CONFIG_MONGODB_URL: mongodb://root:example@mongo:27017/

### Start the Stack
    docker compose -f stack.yml up -d

### Mongo Express
    http://localhost:8081
  
## Create the API Application

Reference: https://blog.logrocket.com/how-to-set-up-node-typescript-express/

If using existing repo, simply run npm install and skip to "Run  the API Application."

    npm install

### Create package.json
    npm init -y

### Install dependencies
    npm i express
    npm i -D typescript @types/express @types/node
    npm i -D nodemon ts-node

### Setup tsconfig.json for Typescript
    npx tsc --init

### Add an index.ts

    import express, { Express, Request, Response } from "express";

    const app: Express = express();
    const port = 3000;

    app.get("/", (req: Request, res: Response) => {
        res.send("Express + TypeScript Server");
    });

    app.listen(port, () => {
        console.log(`[server]: Server is running at http://localhost:${port}`);
    });

## Run the api application
    npx ts-node index.ts

    Wow, it tunnels to localhost with VSCode Remote tools
    Simply open http://localhost:3000 or http://raspberrypi.local:3000 in a browser.

## Add scripts
    "scripts": {
        "build": "npx tsc",
        "start": "node api/index.js",
        "dev": "nodemon api/index.ts"
    },

## Run nodemon to watch for changes
    npm run dev